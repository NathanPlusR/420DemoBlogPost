from __init__ import create_instance_app
from config import Config_Dev, Config_Prod

if __name__=='__main__': 
	app = create_instance_app()
	app.run(port=5003, debug=Config_Dev.DEBUG) # run appin port 5003 and appshould be debuggable