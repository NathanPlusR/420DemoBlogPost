from flask import Flask
from markupsafe import escape
import secrets
from config import Config_Dev, Config_Prod
from flask_login import LoginManager
# from auth.user import User

# login_manager = LoginManager()
# @login_manager.user_loader
# def load_user(author_name) :
#     user = User(author_name)
#     return user

def create_instance_app(class_config=Config_Dev) :

    app = Flask(__name__)

    # login_manager.init_app(app)

    from main.routes import main
    from post.routes import post
    from auth.routes import auth

    app.register_blueprint(main)
    app.register_blueprint(post)
    app.register_blueprint(auth)

    app.secret_key = class_config.SECRET_KEY

    return app