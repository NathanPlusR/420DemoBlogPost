create table author (
    author_id NUMBER(4) GENERATED ALWAYS AS IDENTITY,
    author_name VARCHAR2(25) NOT NULL UNIQUE,
    email VARCHAR2(50) NOT NULL UNIQUE,
    password VARCHAR(80) NOT NULL,
    occupation VARCHAR2(80),
    author_image VARCHAR2 (80) NOT NULL default('default_user.png')
    active NUMBER(1) default (1)
    constraint author_pk PRIMARY KEY (author_id)
)

create table posts (
    post_id NUMBER (4),
    author_id NUMBER (4) NOT NULL,
    title VARCHAR2 (144) NOT NULL,
    content VARCHAR2(1000) NOT NULL,
    date_posted DATE DEFAULT sys_date,

    constraint post_pk PRIMARY KEY (post_id),
    foreign key author_id references author (author_id)
)


--Author inserts
INSERT INTO author (active, author_name, email, pasword, occupation, author_image)
    values(1, "John", "John@john", "1234", "yayaya")
INSERT INTO author (active, author_name, email, pasword, occupation, author_image)
    values(1, "Maya", "May@john", "dolphin street", "wowzanouioe")

--Post Inserts
