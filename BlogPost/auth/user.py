from flask_login import UserMixin
from BlogPost.qdb.database import Database

class User(UserMixin) :
    def __init__(self, author_name) :
            user = Database().get_one_author(f"author_name='{author_name}'")
            self.author_id = user[0]
            self.author_name = user[1]
            self.email = user[2]
            self.password = user[3]
            self.occupation = user[4]
            self.author_image = user[5]
            self.active = user[6]

    def get_id(self):
          return self.author_name