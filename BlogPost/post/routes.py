# from blog_post_app import app
from flask import Flask, request, render_template, redirect, abort, Blueprint
from flask_login import login_required

post = Blueprint('post', __name__, template_folder = 'templates')

@post.route('/post/<int:post_id>')
def post_page(post_id):
    # post_info = get_post(int(post_id))
    context = {'page_title':'post', 'main_heading': f'post {post_id} info' }
    return render_template('post.html', page = context)

@login_required
@post.route('/list_all_posts')
def allposts():
    # post_info = get_post(int(post_id))
    context = {'page_title':'post', 'main_heading': 'all posts' }
    return render_template('list_all_posts.html', page = context, posts = post_list)
