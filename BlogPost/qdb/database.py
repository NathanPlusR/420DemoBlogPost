import os
import oracledb
from BlogPost.config_db import host, usr, pw, sn

class Database:
    def __init__(self, autocommit=True):
        self.__connection = self.__connect()
        self.__connection.autocommit = autocommit

    def __connect(self):
        '''Connects to database using config_db's global variables'''
        return oracledb.connect(user=usr, password=pw, host=host,  service_name=sn) 
    
    def  db_conn (self): 
        return self.__connection

    def __run_file(self, file_path):
        statement_parts = []
        with self.__connection.cursor() as cursor:
            # pdb.set_trace()
            with open(file_path, 'r') as f:
                for line in f:
                    if line[:2]=='--': continue
                    statement_parts.append(line)
                    if line.strip('\n').strip('\n\r').strip().endswith(';'):
                        statement = "".join( statement_parts).strip().rstrip(';')
                        if statement:
                            try:
                                # pdb.set_trace()
                                cursor.execute(statement)
                            except Exception as e:
                                print(e)
                        statement_parts = []

    
    def add_address(self, address):
        '''Add an address to the DB for the given Address object'''
        pass

    def get_address(self, name):
        '''Returns an Address object based on the provided name'''
        pass

    def get_addresses(self):
        '''Returns all Address objects in a list'''
        pass

    def close(self):
        '''Closes the connection'''
        if self.__connection is not None:
            self.__connection.close()
            self.__connection = None

    def get_cursor(self):
            for i in range(3):
                try:
                    return self.__connection.cursor()
                except Exception as e:
                    # Might need to reconnect
                    self.__reconnect()

    def __reconnect(self):
        try:
            self.close()
        except oracledb.Error as f:
            pass
        self.__connection = self.__connect()

    

    def run_sql_script(self, sql_filename):
        if os.path.exists(sql_filename):
            self.__connect()
            self.__run_file(sql_filename)
            self.close()
        else:
            print('Invalid Path')
            
    def get_one_author(self, cond):
        ''' method to list all (if cond is True) or some address books based on cond'''
        try:
            records = []
            with self.get_cursor() as cursor :
                qry = f"select * from AUTHOR where {str(cond)}"
                cursor.execute(qry)
                results = cursor.fetchall()
                for row in results:
                    records.append(row)
                self.__connection.commit()
            return records
        except oracledb.Error as e:
            print(e)
            pass
# ----------------------------------
# ------- DML DB Operations  
    def get_addresses(self, cond):
        ''' method to list all (if cond is True) or some address books based on cond'''
        try:
            records = []
            with self.get_cursor() as cursor :
                
                if cond == True :
                    qry = 'select * from ADDRESS_BOOK'
                else :
                    qry = f"select * from ADDRESS_BOOK where user_name = '{str(cond)}'"
                cursor.execute(qry)
                results = cursor.fetchall()
                for row in results:
                    records.append(row)
                self.__connection.commit()
            return records
        except oracledb.Error as e:
            print(e)
            pass
# ----------------------
    def add_new_address(self, user_name, street, city, province):
        '''  method to add a new address, data coming fro a user input form'''
        try:
            with self.get_cursor() as cursor :
                insert_qry = "INSERT INTO ADDRESS_BOOK (user_name, street, city, province) values(:user_name, :street, :city, :province)"
                cursor.execute(insert_qry, (user_name, street, city, province))
                self.__connection.commit()
        except oracledb.Error as e:
            print(e)
            pass
# ----------------------
    def get_address_field(self, field, cond):
        '''  method to get the column field based on condition'''
        # pass 
      
# ----------------------                   
    def get_notes_user(self, author_id):
        ''' method to list all notes of a given user '''
        try:
            records = []
            with self.get_cursor() as cursor :
                qry = f'select * from NOTE where author_id = {author_id}'
                cursor.execute(qry)
                results = cursor.fetchall()
                for row in results:
                    records.append(row)
                self.__connection.commit()
            return records
        except oracledb.Error as e:
            print(e)
            pass
# ----------------------                   
# ----------------------------
#  1.	Create two global variables:
# •	an instance of Database class that you call db.

# 2.	Call run_sql_script on database.sql  if the script (database.py) is run in isolation.
if __name__ == '__main__':
    db = Database()
    db.run_sql_script("./schema.sql")