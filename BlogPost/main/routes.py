from flask import Flask, request, render_template, redirect, abort, Blueprint
from markupsafe import escape

main = Blueprint('main', __name__, template_folder = 'templates')

@main.route('/')
@main.route('/home')
def home():
    context = {'page_title':'home', 'main_heading': 'home page' }
    return render_template('home.html', page = context)

@main.route('/about')
@main.route('/a-propos')
def about():
    context = {'page_title':'about', 'main_heading': 'about page' }
    return render_template('about.html', page = context)

@main.errorhandler(404)
def page_not_found(e) :
    return '<h1>grah<h1>', 404
     