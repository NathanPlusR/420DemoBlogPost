from flask import Flask, request, render_template, redirect, abort, Blueprint, url_for
from markupsafe import escape
from forms import RegistrationForm, LoginForm
from BlogPost.qdb.database import db
from flask_bcrypt import Bcrypt
from user import User
from flask_login import login_user, logout_user, current_user, login_required
from flask import flash

auth = Blueprint('auth', __name__, template_folder = 'templates')

@auth.route('/register')
def register():
    reg_form = RegistrationForm()
    if reg_form.validate_on_submit():
        user_exist = db.get_one_author(f"username='{reg_form.name.data}'")
        if user_exist :
            db.add_new_address(user_name = reg_form.name.data,
                                street = reg_form.street.data,
                                city = reg_form.city.data,
                                province = reg_form.province.data)
            flash('Successfully added address!', 'success')
            return redirect(url_for('addressbook', 
                                    name=reg_form.name.data))
    context = {'page_title':'Register'}
    return render_template("addresses.html", 
                           page=context, 
                           form=reg_form)

    context = {'page_title':'home', 'main_heading': 'home page' }
    
    return render_template('register.html', page = context)

@auth.route('/login')
def login():
    login_form = LoginForm()
    context = {'page_title':'Login'}
    if login_form.validate_on_submit():
        user_exist = db.get_one_author(f"username='{login_form.name.data}'")
        if user_exist :
            stored_passwd = user_exist[3]
            pass_correct = Bcrypt.check_password_hash(stored_passwd, login_form.password.data)
            if pass_correct :
                user = User(login_form.name)
                login_user(user)
                flash('success')
                return redirect(url_for('auth.author'))   
    
    return render_template('login.html', page = context)

@auth.route('/author/<int:post_id>')
def author(author_id):
    # post_info = get_post(int(post_id))
    context = {'page_title':'post', 'main_heading': f'post {author_id} info' }
    return render_template('post.html', page = context)


@auth.route('/logout')
def logout() :
    logout_user()
    return redirect(url_for('login'))