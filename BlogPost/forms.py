from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField , EmailField 
from wtforms.validators import DataRequired, Length, Email, EqualTo
from flask_bcrypt import Bcrypt

# b = Bcrypt()
# pass_text = '12'
# b.generate_password_hash(pass_text)
# hashed_pass = b.generate_password_hash(pass_text)
# hashed_pass = b.generate_password_hash(pass_text).decode('utf-8')
# check_pass = b.check_password_hash(hashed_pass, '11')
# b.check_password_hash(hashed_pass, '12')

class RegistrationForm(FlaskForm) :
	name = StringField ('name', 
			validators=[DataRequired(), Length(min=2, max=25)])
	email = EmailField('email',
			validators=[DataRequired(), Email(), Length(min=2, max=100)])
	password = PasswordField('password', validators=[DataRequired(), Length(min=2, max=100)])
	confirm_password = PasswordField('confirm_password',
			validators=[DataRequired(), EqualTo('password')])

	submit = SubmitField ('register')

class LoginForm(FlaskForm) :
	name = StringField ('name', 
			validators=[DataRequired(), Length(min=2, max=25)])
	email = EmailField('email',
			validators=[DataRequired(), Email(), Length(min=2, max=100)])
	password = PasswordField('password', validators=[DataRequired(), Length(min=2, max=100)])

	submit = SubmitField ('login')